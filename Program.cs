﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.Speech.Synthesis;

namespace Sam
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create intigers for use later
            int pollDelay, countToAnnouce;
            float currentCPU;
            float availMem;

            // Initialize all Functionality

            // Poll CPU Load
            PerformanceCounter useCPU = new PerformanceCounter("Processor Information", "% Processor Time", "_Total");
            // Poll Available Mem
            PerformanceCounter useMem = new PerformanceCounter("Memory", "Available MBytes");
            // Create Vocalization
            SpeechSynthesizer synth = new SpeechSynthesizer();

            Console.WriteLine("Hello, Welcome to Microsoft Sam Perfomance Monitor!");
            // Somewhat annoying to hear and wait for, enable if you would like
            //synth.Speak("Hello, welcome to Microsoft Sam Performance Monitor!");

            Console.WriteLine("Please enter the frequency you would like to poll system stats. (seconds)");
            pollDelay = int.Parse(Console.ReadLine()) *1000;

            Console.WriteLine("Please enter how many times you would like to poll before annoucing.");
            countToAnnouce = int.Parse(Console.ReadLine());

            while (true)
            {
                // Use a for loop here, it's declared like so.
                // for(declare our increment here; what is run as a condition each time; what is done each time to poke the increment)
                for (int i = 0; i < countToAnnouce; i++)
                {
                    currentCPU = useCPU.NextValue();
                    availMem = useMem.NextValue() / 1024;

                    Console.WriteLine("=================================");
                    Console.WriteLine("CPU Load        : % {0}", currentCPU);
                    Console.WriteLine("Available Memory: {0} GB", availMem);
                    Console.WriteLine("=================================");
                    Console.WriteLine("");
                    Thread.Sleep(pollDelay);
                }

                currentCPU = useCPU.NextValue();
                availMem = useMem.NextValue() / 1024;

                Console.WriteLine("=================================");
                Console.WriteLine("Voice Announcing the following:");
                Console.WriteLine("");
                Console.WriteLine("CPU Load        : % {0}", currentCPU);
                Console.WriteLine("Available Memory: {0} GB", availMem);
                Console.WriteLine("=================================");
                Console.WriteLine("");

                synth.Speak(String.Format("Your current CPU Load is {0} percent.", (int)currentCPU));
                synth.Speak(String.Format("You have {0} gigabytes available memory.", (int)availMem));
            }
        }
    }
}